# - - - - - - - - - - #
# TYDREX FILE BACKUPS #
# RAN DAILY AT 3 A.M. #
#  UPDATED: 3/1/2015  #
# - - - - - - - - - - #

#!/bin/sh

DATE=`date +%m-%d-%y-%H%M`
SES="tmux send-keys -t tydrex"
NAME="tydrex"

# - - - FILES - - - #
${SES} 'broadcast &7&oServer backup is &9&obeginning&7&o, please don''t mind any lag.' C-m ;
${SES} 'save-all' C-m ;
sleep 3 ;
${SES} 'save-off' C-m ;
tar czf /home/backups/backup_${NAME}_${DATE}.tar -C / /home/${NAME}
gzip /home/backups/backup_${NAME}_${DATE}.tar
${SES} 'save-on' C-m ;

# - - - MYSQL - - - #
mysqldump -u root -p#redacted# ${NAME} | gzip > /home/backups/dbbackup_${NAME}_${DATE}.bak.gz

${SES} 'broadcast &7&oServer backup &9&ocompleted&7&o. ;)' C-m ;

# - - - RM OLD BACKUPS - - - #
find /home/backups/ -mtime +15 -exec rm -rf {} \;

#end