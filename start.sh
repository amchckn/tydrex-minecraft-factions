# - - - - - - - - - - - #
# TYDREX SERVER STARTUP #
#   RAN ALL THE TIME    #
#   UPDATED: 3/1/2015   #
# - - - - - - - - - - - #

#!/bin/bash

while true ; do

echo ""
echo "            ## - - - - - - - - - - - - - - - - - - ##"
echo "            ##                                     ##"
echo "            ##         AUTO STARTUP SCRIPT         ##"
echo "            ##                                     ##"
echo "            ##                 ^3^                 ##"
echo "            ##                                     ##"
echo "            ##     Starting in 3 second(s)...      ##"
sleep 1
echo "            ##     Starting in 2 second(s)...      ##"
sleep 1
echo "            ##     Starting in 1 second(s)...      ##"
echo "            ##                                     ##"
echo "            ## - - - - - - - - - - - - - - - - - - ##"
echo ""
sleep 1

        java -server -noverify -XX:+TieredCompilation -XX:CompileThreshold=300 -Xmx8G -XX:+UseConcMarkSweepGC -XX:+AggressiveOpts -XX:MaxGCPauseMillis=20 -jar paperspigot.jar -o true

done

