# - - - - - - - - - - - #
# TYDREX SERVER RESTART #
#  RAN DAILY AT 3 A.M.  #
#   UPDATED: 3/1/2015   #
# - - - - - - - - - - - #

#!/bin/sh

TMUX="tmux send-keys -t tydrex"

# - - - - - - #

${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&oten &7&ominutes.' C-m ;
sleep 300 ;
${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&ofive &7&ominutes.' C-m ;
sleep 240 ;
${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&oone &7&ominute.' C-m ;
sleep 30 ;
${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&othirty &7&oseconds.' C-m ;
sleep 20 ;
${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&oten &7&oseconds.' C-m ;
sleep 5 ;
${TMUX} 'broadcast &7&oThe server is scheduled to restart in &9&ofive &7&oseconds.' C-m ;
${TMUX} 'plugman unload combattagplus' C-m ;
sleep 5 ;
${TMUX} 'stop' C-m ;

#end
